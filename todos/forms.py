from django.forms import ModelForm
from todos.models import TodoList


class ToDoForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]
