from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import ToDoForm


# Create your views here.
def todo_list_list(request):
    list = TodoList.objects.all()
    context = {
        "list": list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": detail,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = ToDoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("detail.html", id=form.id)
    else:
        form = ToDoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)
